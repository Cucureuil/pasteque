
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * Panel contenant la m�thode graphique
 *  @author Chlo� MASONI, Oc�ane BERTHE
 */
class PanelGraphique extends JPanel
{
	/**
	 * ATTRIBUTS
	 */
	private Grille g;

	/**
	 * Constructeur
	 * @param grille La grille � dessiner
	 */
	public PanelGraphique(Grille grille){
		g=grille;
		this.repaint();
	}

	/**
	 * M�thode graphique qui permet l'affichage des damiers et des pions
	 */
	public void paintComponent(Graphics g)
	{
		for(int i=0;i<this.g.getTablier().length;i++)
		{
			for(int j=0;j<this.g.getTablier()[i].length;j++)
			{
				if(this.g.getTablier()[i][j]==' ') {
					g.setColor(Color.BLACK);
				} else {
					if((i+j)%2==0) g.setColor(Color.black);
					else g.setColor(Color.white);
					g.fillRect(j*25,i*25,25,25);
					if(this.g.getTablier()[i][j]=='o'){
						g.setColor(Color.red);
						g.fillOval(((12+j*25)-3),(12+ i*25-3), 6, 6);
					}
				}
			}
		}
		g.setColor(Color.black);
		g.drawRect(0, 0, 25*this.g.getTablier().length, 25*this.g.getTablier().length);
	}

	/**
	 * Getter grille
	 * @return Grille, la grille
	 */
	public Grille getG() {
		return g;
	}
}