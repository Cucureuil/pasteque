

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * R�solveur de grilles de solitaire
 * Avec affichage graphique
 * @author Chlo� MASONI, Oc�ane BERTHE
 */
public class SoloNoble {

	/**
	 * @param jeu, Grille correspondant � l'�tat du jeu en cours de r�solution
	 * @param listDeplacements, ArrayList comptant toutes les �tapes de la r�alisation du SoloNoble (pour permettre l'affichage � la fin)
	 */
	private Grille jeu ;
	private ArrayList<Grille> listeDeplacements ;

	/**
	 * Constructeur de SoloNoble
	 * @param s String, le chemin du fichier � charger
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public SoloNoble(String s) throws FileNotFoundException, IOException {
		jeu = new Grille(s) ;
		listeDeplacements = new ArrayList<Grille>() ; 
		listeDeplacements.add(new Grille(jeu)) ;
	}

	/**
	 * Main
	 * @param args[0] le chemin du fichier � charger
	 * @param args[1] attendu : true/false, pour afficher ou non les solutions graphiquement (par defaut l'affichage est � true)
	 */
	public static void main(String[] args) {
		//Le try est seulement ici pour permettre l'arr�t du programme en cas de probl�me de lecture
		//Sinon �a cr�e juste une erreur
		try {
			SoloNoble s ;
			// V�rifie si un chemin de fichier a �t� pr�cis� en argument
			if(args.length!=0)
				s = new SoloNoble(args[0]);
			else
				s = new SoloNoble("Grille2.txt") ;
		
			// Ex�cution du r�solveur de grille de solitaire
			s.resoudreSoloNoble(s.jeu.getNbPions());
			
			// Configuration de la Fenetre Graphique
			if(args.length==0 || args[1]=="true"){
				JFrame fenetre=s.configurerFenetre();
			} 
		}
		catch (FileNotFoundException e) {
			System.out.println("Le fichier sp�cifi� est introuvable");
		}
		catch(IOException e) {
			System.out.println("Probl�me de lecture dans le fichier");
		}
	}

	/**
	 * M�thode permettant de r�soudre le jeu
	 * @param billes int, le nombre de billes dans le SoloNoble
	 */
	public boolean resoudreSoloNoble(int billes) {
		boolean infructueux ;
		Point pLibre ;
		// condition de r�solution d'une grille de solitaire
		if(billes==1) {
			// Affiche toutes les �tapes de r�solution grille par grille
			for(int i=0 ; i<listeDeplacements.size() ; i++) {
				System.out.println("Etape " +i);
				System.out.println(listeDeplacements.get(i));
				System.out.println("----------------------------------------");
			}
			System.out.println("Jeu r�solu !");
			infructueux = false ;
		}			
		else {
			infructueux = true ;
			// Parcours des points libres disponibles sur la plateau
			for(int j=0 ; j<jeu.getPoints_libres().size() ; j++) {
				pLibre = jeu.getPoints_libres().get(j) ;
				for(int i=0 ; i<jeu.deplacementPossible(pLibre).size() && infructueux; i++) {
					// D�placement d'un pion parmis les pions libres
					jeu.deplacer(jeu.deplacementPossible(pLibre).get(i), pLibre);
					// Ajout de la grille apr�s le d�placement du pion dans la liste des d�placements r�alis�s
					listeDeplacements.add(new Grille(jeu)) ;
					infructueux = resoudreSoloNoble(jeu.getNbPions()) ;
					// On v�rifie si le d�placement � d�bouch� sur un succ�s
					if(infructueux) {
						listeDeplacements.remove(jeu) ;
						jeu = new Grille(listeDeplacements.get(listeDeplacements.size()-1)) ;
					}
				}
			}
		}
		return infructueux ;
	}
	
	/**
	 * M�thode graphique, g�re la Frame et les panels
	 * @return JFrame, g�rant l'affichage graphique des solutions
	 */
	public JFrame configurerFenetre(){
		// PARAMETRES DE LA FENETRE
		JFrame f=new JFrame();
		f.setSize(410,700);
		f.setResizable(false);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		f.setTitle("Solitaire");
		
		// Compl�tion de la liste de plateau d'apr�s les solutions r�cup�r�es apr�s l'execution du r�solveur de solitaire
		ArrayList<PanelGraphique> liste_Plateau=this.conversionListeGrilleToListePlateau();
		
		// PANEL PRINCIPAL
		JPanel panneauP=new JPanel();
		panneauP.setPreferredSize(new Dimension(400,500));
		panneauP.setLayout(new BorderLayout());
		
		// PANEL CENTRAL
		JPanel panneauC=new JPanel();
		panneauC.setLayout(new GridLayout(liste_Plateau.size()/3,1));
		
		// Panneau sup�rieur 
		JPanel panneauSup=new JPanel();
		panneauSup.setLayout(new GridLayout(2,1));
		JLabel titre=new JLabel("SOLUTION");
		titre.setHorizontalAlignment(titre.CENTER);
		JLabel nbSol=new JLabel("Nombre d'�tapes : "+String.valueOf(this.listeDeplacements.size()));
		nbSol.setHorizontalAlignment(nbSol.CENTER);
		JLabel Nonsol=new JLabel("PAS DE SOLUTIONS TROUVEES");
		Nonsol.setHorizontalAlignment(Nonsol.CENTER);
		if(liste_Plateau.size()<=1){
			panneauSup.add(Nonsol);
		} else {
			panneauSup.add(titre);
		}
		
		
		panneauSup.add(nbSol);
		
		// Panel qui repr�sente les lignes de l'affichage une � une
		JPanel ligne;
		Color c=new Color(100,100,100);
		for(int i=0;i<liste_Plateau.size();i=i+3){
			ligne=new JPanel();
			ligne.setBackground(new Color(c.getBlue()+i*10,c.getGreen()+i*10,c.getRed()+i*10));
			ligne.setLayout(new GridLayout(1,3));
			for(int j=0;j<3;j++){
				if(i+j<liste_Plateau.size()) ligne.add(liste_Plateau.get(i+j));
			}
			panneauC.add(ligne);
		}
		// ajout des pannels au pannel principal
		panneauP.add(panneauSup,BorderLayout.NORTH);
		panneauP.add(panneauC,BorderLayout.CENTER);
		// G�re la barre de scroll de la fen�tre
		JScrollPane scrollPane = new JScrollPane(panneauP);
		f.add(scrollPane);
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		
		return f;
	}
	
	/**
	 * Converti la liste de Grille en liste de Plateau (sert � l'affichage graphique des solutions)
	 * @return ArrayList<Plateau>
	 */
	public ArrayList<PanelGraphique> conversionListeGrilleToListePlateau(){
		ArrayList<PanelGraphique> plateaux=new ArrayList<PanelGraphique>();
		for(int i=0; i<this.listeDeplacements.size();i++){
			plateaux.add(new PanelGraphique(this.listeDeplacements.get(i)));
		}
		return plateaux;
	}
}

