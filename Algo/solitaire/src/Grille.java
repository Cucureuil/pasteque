

import java.awt.Point;
import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Mod�lise une grille � partir d'un fichier .txt contenant un tablier de solitaire
 *  @author Chlo� MASONI, Oc�ane BERTHE
 */
public class Grille {

	/**
	 * ATTRIBUTS
	 * nbPions : le nombre de pion sur la grille
	 * points_libres : points lib�r�s, contient �galement le point libre � l'origine
	 */
	private char[][] tablier;
	private int nbPions;
	private ArrayList<Point> points_libres;

	/**
	 * Construit une grille � partir d'un fichier .txt
	 * @param cheminFichier String, le chemin source du fichier � charger
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public Grille(String cheminFichier) throws FileNotFoundException, IOException{
		this.tablier=chargerFichier(cheminFichier);


		// initialiation de la liste de point libre
		this.points_libres=this.trouverEmplacementLibres();

		// initialise le nombre de pion pr�sent sur le tablier par d�faut
		this.nbPions=this.calculNbPion();
	}

	public Grille(Grille g) {
		tablier = new char[g.tablier.length][g.tablier[0].length] ;
		for(int i=0 ; i<g.tablier.length ; i++)
			for(int j=0 ; j<g.tablier[i].length ; j++)
				tablier[i][j] = g.tablier[i][j] ;

		points_libres = new ArrayList<Point>() ;
		for(Point p : g.points_libres)
			this.points_libres.add(p) ;
		this.nbPions = g.nbPions ;
	}

	/**
	 * GETTERS
	 */
	public char[][] getTablier() {
		return tablier;
	}

	public int getNbPions() {
		return nbPions;
	}

	public ArrayList<Point> getPoints_libres() {
		return points_libres;
	}

	/**
	 * M�thode qui charge un fichier et stock le contenu dans un tableau de char
	 * @param chemin String, le chemin d'acc�s au fichier � charger
	 * @return char[][] , le contenu du fichier
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public char[][] chargerFichier(String chemin) throws FileNotFoundException, IOException{
		char[][] tab={};
		int caractLu;
		BufferedReader fichier;
		Path path=Paths.get(chemin);

		// lecture du fichier et calcul de la taille du tablier en ligne et en colonne
		fichier = new BufferedReader(new FileReader(chemin));
		String s = fichier.readLine();
		int nbColonne=s.length();
		int nbLigne=(int) Files.lines(path).count();
		// initialisation du tablier
		tab=new char[nbLigne][nbColonne];

		int c;
		char caractere;
		int numero_ligne=0;
		int numero_colonne=0;

		//rempli la 1�re ligne du tableau : comme elle a d�j� �t� lue dans le fichier, inutile de la relire
		do {
			for(int i=0 ; i<nbColonne ; i++) {
				tab[numero_ligne][numero_colonne] = s.charAt(i) ;
				numero_colonne++ ;
			}
			numero_colonne = 0 ;
			numero_ligne++ ;
		} while((s=fichier.readLine())!=null) ; //ici on lit pour passer � la ligne suivante et refaire le do

		return tab;
	}

	/**
	 * Retourne le nombre de pion pr�sent sur le tablier
	 * @return int, le nombre de pion
	 */
	public int calculNbPion(){
		int nbpion=0;
		// Parcours du tablier
		for(int lignes=0; lignes<this.tablier.length;lignes++){
			for(int colonnes=0;colonnes<this.tablier[lignes].length;colonnes++){
				// V�rification qu'il s'agit bien d'un espace sans pion
				if(this.tablier[lignes][colonnes]=='o'){
					nbpion++;
				}
			}
		}
		return nbpion;
	}

	/**
	 * Retourne les emplacements libres, sous forme de point, c'est � dire les emplacements appartenant � la grille mais sans pion dessus
	 * @return ArrayList<Point>, les emplacements libres
	 */
	public ArrayList<Point> trouverEmplacementLibres(){
		ArrayList<Point> emplacements=new ArrayList<Point>();

		Point p=null;
		// Parcours du tablier
		for(int lignes=0; lignes<this.tablier.length;lignes++){
			for(int colonnes=0;colonnes<this.tablier[lignes].length;colonnes++){
				// V�rification qu'il s'agit bien d'un espace sans pion
				if(this.tablier[lignes][colonnes]=='.'){
					// Cr�e un point o� l'emplacement est libre � partir de sa position dans le tableau
					p=new Point(colonnes,lignes);
					emplacements.add(p);
				}
			}
		}
		return emplacements;
	}

	/**
	 * V�rifie et retourne les pions qui peuvent aller compler le point pass� en param�tre
	 * @param pLibre Point, la position point libre
	 * @return ArrayList<Point> la liste des pions qui peuvent se d�placer vers le point libre
	 */
	public ArrayList<Point> deplacementPossible(Point pLibre){
		ArrayList<Point> points = new ArrayList<Point>();
		int x = (int)pLibre.getX();
		int y = (int)pLibre.getY();

		if(x-2>=0) {
			if(this.tablier[y][x-1]=='o' && this.tablier[y][x-2]=='o'){
				points.add(new Point(x-2, y)) ;
			}
		}
		if(x+2<this.tablier[y].length){
			if(this.tablier[y][x+1]=='o' && this.tablier[y][x+2]=='o') {
				points.add(new Point(x+2, y)) ;
			}
		}
		if(y-2>=0){
			if(this.tablier[y-1][x]=='o' && this.tablier[y-2][x]=='o') {
				points.add(new Point(x, y-2)) ;
			}
		}
		if(y+2<this.tablier.length) {
			if(this.tablier[y+1][x]=='o' && this.tablier[y+2][x]=='o') {
				points.add(new Point(x, y+2)) ;
			}
		}
		return points;
	}

	/**
	 * Retire un pion 
	 */
	public void enleverPion(Point p){
		this.points_libres.add(p);
		this.tablier[p.y][p.x]='.';
		this.nbPions--;
	}

	/**
	 * Ajoute un pion 
	 */
	public void ajouterPion(Point p){
		if(this.points_libres.contains(p)){
			this.points_libres.remove(p);
			this.tablier[p.y][p.x]='o';
			this.nbPions++;
		}	
	}

	/**
	 * D�place le pion actif
	 * @param depart
	 * @param arrivee
	 */
	public void deplacer(Point depart,Point arrivee){
		//on retire le pion situ� entre le d�part et l'arrivee
		if(depart.x==arrivee.x){
			if(depart.y<arrivee.y){
				this.enleverPion(new Point(depart.x,depart.y+1));
			} else {
				this.enleverPion(new Point(depart.x,arrivee.y+1));
			}
		} else {
			if(depart.x<arrivee.x){
				this.enleverPion(new Point(depart.x+1,depart.y));
			} else {
				this.enleverPion(new Point(arrivee.x+1,depart.y));
			}
		}
		//puis on d�place le pion qui a "mang�" l'autre
		this.enleverPion(depart);
		this.ajouterPion(arrivee);
	}

	/**
	 * Affichage des caract�ristiques d'une grille
	 */
	public String toString(){
		String s="";
		for(int i=0;i<this.tablier.length;i++){
			s+="| ";
			for(int j=0;j<this.tablier[i].length;j++){
				s+=this.tablier[i][j]+" | ";
			}
			s+="\n";
		}
		return s;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grille other = (Grille) obj;
		if (nbPions != other.nbPions)
			return false;
		if (points_libres == null) {
			if (other.points_libres != null)
				return false;
		} else if (!points_libres.equals(other.points_libres))
			return false;
		if (!Arrays.deepEquals(tablier, other.tablier))
			return false;
		return true;
	}


}
