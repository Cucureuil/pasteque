package modele;

import java.util.ArrayList;
import java.util.Observable;
import pizza.*;

public class ModeleCommande extends Observable{

	/**
	 * ATTRIBUTS
	 */
	private int nbPizza=0;
	private ArrayList<Pizza> listPizza;
	private int numPizzaCourante;
	private double prixCommande;

	/**
	 * Constructeur 
	 */
	public ModeleCommande(){
		this.listPizza=new ArrayList<Pizza>();
		this.prixCommande=0;
		this.numPizzaCourante=0;
	}

	/**
	 * Ajotue une pizza � la lsite de pizza de la commande
	 * @param s String, la base de la pizza a ajouter
	 */
	public void ajouterPizza(String s){
		if(this.nbPizza<4){
			if(s.equals("Tomate")){
				this.listPizza.add(new PizzaTomate());
				nbPizza++;
				numPizzaCourante++;
			}
			if(s.equals("Creme")){
				this.listPizza.add(new PizzaCreme());
				nbPizza++;
				numPizzaCourante++;
			}
		}
		setChanged();
		notifyObservers();
	}

	/**
	 * Calcul le prix total de la commadne en cours
	 * @return double, le prix de la commande
	 */
	public double calculPrixCommande(){
		double prix=0;
		for(int i=0;i<this.listPizza.size();i++){
			prix+=this.listPizza.get(i).cout();
		}
		return prix;
	}

	/**
	 * S�lectionne une pizza
	 * @param num int, le numero de la pizza � s�lectionner
	 */
	public void setNumPizzaCourante(int num){
		if(num>=0 && num<=4){
			this.numPizzaCourante=num;
		}
		setChanged();
		notifyObservers();
	}

	/**
	 * GETTER
	 */
	
	public int getNbPizza() {
		return nbPizza;
	}

	public ArrayList<Pizza> getListPizza() {
		return listPizza;
	}

	public int getNumPizzaCourante() {
		return numPizzaCourante;
	}
	
	
}
