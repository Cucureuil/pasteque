package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modele.ModeleCommande;

public class ControlIngredient implements ActionListener{

	/**
	 * ATTRIBUTS
	 */
	private ModeleCommande modele;
	private String[] ingredients;
	
	/**
	 * CONSTRUCTEURS
	 * @param mc ModeleCommande
	 */
	public ControlIngredient(ModeleCommande mc){
		this.modele=mc;
	}
	
	/**
	 * Gestion de l'initialisation de la commane par l'interface graphique
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("ControlIngredient");
	}

	
}
