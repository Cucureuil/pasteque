package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modele.ModeleCommande;
import pizza.PizzaCreme;

public class ControlDebutCommande implements ActionListener{

	/**
	 * ATTRIBUTS
	 */
	private ModeleCommande modele;

	/**
	 * CONSTRUCTEURS
	 * @param mc ModeleCommande
	 */
	public ControlDebutCommande(ModeleCommande mc){
		this.modele=mc;
	}

	/**
	 * Gestion de l'initialisation de la commane par l'interface graphique
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals(" Ajouter une pizza fond creme ")){
			modele.ajouterPizza("Creme");
		}
		
		if(e.getActionCommand().equals(" Ajouter une pizza fond tomate ")){
			modele.ajouterPizza("Tomate");
		}
	}
}
