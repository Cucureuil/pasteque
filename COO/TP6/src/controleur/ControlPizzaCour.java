package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import modele.ModeleCommande;

public class ControlPizzaCour implements MouseListener{

	/**
	 * ATTRIBUTS
	 */
	private ModeleCommande modele; 
	
	/**
	 * CONSTRUCTEUR
	 * @param mc ModeleCommande
	 */
	public ControlPizzaCour(ModeleCommande mc){
		this.modele=mc;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		this.modele.setNumPizzaCourante(e.getX()/225);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

}
