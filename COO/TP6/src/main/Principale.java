/**
 * 
 */
package main;

import vue.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.ModeleCommande;

/**
 * Classe � modifier selon l'�nonc� du TP
 *
 */
public class Principale {

	public static void main(String[] args) {

		ModeleCommande mc=new ModeleCommande();
		VueCommIm f=new VueCommIm(mc);
	}

}
