package pizza;
import gestionImage.*;

public interface Pizza {

	public MyImage getPizzalm();
	
	public double cout();
	
	public String getDescription();
}
