package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.ControlDebutCommande;
import controleur.ControlPizzaCour;
import gestionImage.MyImage;
import javafx.beans.InvalidationListener;
import modele.ModeleCommande;

public class VueCommIm extends JPanel implements Observer {

	/**
	 * ATTRIBUTS
	 */
	private ModeleCommande modele;

	/**
	 * CONSTRUCTEUR
	 * @param mc
	 */
	public VueCommIm(ModeleCommande mc){
		this.modele=mc;

		String [] ingredients={"Fromage","Champignons","Chorizo","Oeuf","Oignons","Olives noires","Olives vertes","Roquette"};
		JButton[] bIngr;

		// JPanel situ� au nord de l'IG contenant les 2 boutons 
		// permettant le choix de la base des pizzas
		JPanel pnord = new JPanel();
		pnord.setPreferredSize(new Dimension(935,50));
		JButton  addPizzaCreme= new JButton(" Ajouter une pizza fond creme ");
		JButton addPizzaTomate= new JButton(" Ajouter une pizza fond tomate ");

		ControlDebutCommande cdc=new ControlDebutCommande(this.modele);
		// AJOUT LISTENER DES BOUTONS NORTH
		addPizzaCreme.addActionListener(cdc);
		addPizzaTomate.addActionListener(cdc);

		pnord.add(addPizzaCreme);
		pnord.add(addPizzaTomate);


		// JPanel au centre de l'IG contenant la vision du choix des pizzas
		// ainsi que les boutons pour ajouter des ingr�dients
		JPanel pcentral= new JPanel(new BorderLayout());
		//--> Le panneau avec la vision des images des pizzas
		//PanneauImages visionComm = new PanneauImages();
		pcentral.add(this,BorderLayout.CENTER);
		//--> Le panneau contenant les boutons des ingredrients
		JPanel pingr= new JPanel(new GridLayout(1,0));
		bIngr= new JButton[8];
		for(int i=0;i<ingredients.length;i++){
			bIngr[i]=new JButton(ingredients[i]);	
			pingr.add(bIngr[i]);
		}					
		pingr.setPreferredSize(new Dimension(935,50));
		pcentral.add(pingr, BorderLayout.SOUTH);


		// JPanel au sud de l'IG dans lequel se trouve l'affichage
		// du contenu de la commande et son prix 
		JPanel psud= new JPanel(new BorderLayout());
		JLabel commtxt= new JLabel("<html><h3>Aucune commande en cours</h3></html>");
		commtxt.setPreferredSize(new Dimension(935,200));
		psud.add(commtxt,BorderLayout.CENTER);
		psud.add(new JLabel("<html><h3>Pas de commande en cours</h3></html>",JLabel.CENTER), BorderLayout.SOUTH);

		// GESTION DES LISTENER DE MOUSE
		ControlPizzaCour cpc=new ControlPizzaCour(this.modele);
		pcentral.addMouseListener(cpc);

		/*************************************
		 * Construction de l'IG dans une JFrame
		 *************************************/
		JFrame frame=new JFrame("Commande de pizzas");
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.add(pnord,BorderLayout.NORTH); 
		frame.add(pcentral,BorderLayout.CENTER);
		frame.add(psud, BorderLayout.SOUTH);
		frame.setSize(new Dimension(935,670));
		frame.setVisible(true);


		// IMPORTANT
		this.modele.addObserver(this);
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		MyImage image;
		//mim.superposer("images/ing_fromage.png");  //enlever le commentaire pour superposer l'ingredient fromage sur la pizza 
		for(int i=0 ; i<this.modele.getListPizza().size() ; i++){
			image=this.modele.getListPizza().get(i).getPizzalm();
			image.dessinerDansComposant(g, (i+1)*25+ i*200,25); //Affiche 4 fois la meme pizza de base dans le JPanel courant
			
			if(i==this.modele.getNumPizzaCourante()) g.drawRect((i+1)*25 + i*200,25,200,200);
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		repaint();
	}


}
