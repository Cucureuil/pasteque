package testEtu;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import activeRecord.DBConnection;
import activeRecord.Film;
import activeRecord.Personne;

/**
 * Test unitaire pour un Film
 */
public class FilmTest {

	/**
	 * ATTRIBUTS
	 */
	Connection conn;
	Personne p1;
	@Before public void initialize() {
	}

	/**
	 * Test creationTable
	 */
	@Test
	public void test_creation_table_film() {
		try {
			Film.deleteTable();

			Film.createTable();

			Connection conn = DBConnection.getConnection("testpersonne") ;
			DatabaseMetaData md = conn.getMetaData();
			ResultSet rs = md.getTables(null, null, null, null);
			boolean presence = false;
			while (rs.next()) {
				String table = rs.getString("TABLE_NAME");
				if( table.equals("film")) presence = true;
			}
			assertEquals("La table film doit �tre pr�sente", true, presence);
		} catch ( Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Test DeleteTable
	 */
	@Test
	public void test_suppression_table_film() {
		try {
			Film.createTable();

			Film.deleteTable();

			Connection conn = DBConnection.getConnection("testpersonne") ;
			DatabaseMetaData md = conn.getMetaData();
			ResultSet rs = md.getTables(null, null, null, null);
			boolean presence = false;
			while (rs.next()) {
				String table = rs.getString("TABLE_NAME");
				if( table.equals("film")) presence = true;
			}
			assertEquals("La table film ne doit pas �tre pr�sente", false, presence);
		} catch ( Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Test saveNew
	 */
	@Test
	public void test_insert_existe_pas_table_film() {
		try {
			Film.deleteTable();

			Film.createTable();

			Connection conn = DBConnection.getConnection("testpersonne") ;
			
			Personne.createTable();
			p1= new Personne("fincher", "steven");
			p1.save();
			
			Film f1=new Film("seven",p1);
			f1.save();

			java.sql.Statement stat=conn.createStatement();
			stat.execute("Select * from film");
			ResultSet rs = stat.getResultSet();
			boolean presence = false;
			while (rs.next()) {
				String titre = rs.getString("TITRE");
				if( titre.equals("seven")) presence = true;
			}
			assertEquals("La table film doit contenir le film", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
		
	}

	/**
	 * Test update
	 */
	@Test
	public void test_insert_existe_table_film() {
		try {
			Film.deleteTable();
			Film.createTable();

			Personne.createTable();
			p1= new Personne("fincher", "steven");
			p1.save();
			
			Connection conn = DBConnection.getConnection("testpersonne") ;
			
			Film f1=new Film("seven",p1);
			f1.save();

			f1.setTitre("test2");
			f1.save();

			java.sql.Statement stat=conn.createStatement();
			stat.execute("Select * from film");
			ResultSet rs = stat.getResultSet();
			boolean presence = false;
			while (rs.next()) {
				String titre = rs.getString("TITRE");
				if( titre.equals("test2")) presence = true;
			}
			assertEquals("La table film doit contenir le film modifi�", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Test findById
	 */
	@Test
	public void test_findById_table_film() {
		try {
			Film.deleteTable();
			Film.createTable();

			Personne.createTable();
			p1= new Personne("fincher", "steven");
			p1.save();
			
			Connection conn = DBConnection.getConnection("testpersonne") ;
			
			Film f1=new Film("seven",p1);
			f1.save();

			boolean presence = false;

			String titre= Film.findById(1).getTitre();
			if( titre.equals("seven")) presence = true;

			assertEquals("La table film doit contenir le film � l'id voulu", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Test findByRealisateur
	 */
	@Test
	public void test_findByRea_table_film() {
		try {
			Film.deleteTable();
			Film.createTable();
			Personne.createTable();
			p1= new Personne("fincher", "steven");
			p1.save();
			Connection conn = DBConnection.getConnection("testpersonne") ;
			
			Film f1=new Film("seven",p1);
			f1.save();

			boolean presence = false;

			ArrayList<Film> films= Film.findByRealisateur(p1);
			for(int i=0;i<films.size();i++){
				String titre = films.get(i).getTitre();
				if( titre.equals("seven")) presence = true;
			}
			
			assertEquals("La table film doit contenir le film du r�alisateur voulu", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}
}
