package testEtu;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import activeRecord.DBConnection;
import activeRecord.Film;
import activeRecord.Personne;

/**
 * Test unitaire pour la classe Personne
 */
public class PersonneTest {

	Connection conn;
	Personne p1,p2;

	/**
	 * Test la m�thode createTable
	 */
	@Test
	public void test_creation_table_personne() {
		try {
			Personne.deleteTable();

			Personne.createTable();

			Connection conn = DBConnection.getConnection("testpersonne") ;
			DatabaseMetaData md = conn.getMetaData();
			ResultSet rs = md.getTables(null, null, null, null);
			boolean presence = false;
			while (rs.next()) {
				String table = rs.getString("TABLE_NAME");
				if( table.equals("personne")) presence = true;
			}
			assertEquals("La table personne doit �tre pr�sente", true, presence);
		} catch ( Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Test la m�thode deleteTable
	 */
	@Test
	public void test_suppression_table_personne() {
		try {
			Personne.createTable();

			Personne.deleteTable();

			Connection conn = DBConnection.getConnection("testpersonne") ;
			DatabaseMetaData md = conn.getMetaData();
			ResultSet rs = md.getTables(null, null, null, null);
			boolean presence = false;
			while (rs.next()) {
				String table = rs.getString("TABLE_NAME");
				if( table.equals("personne")) presence = true;
			}
			assertEquals("La table personne ne doit pas �tre pr�sente", false, presence);
		} catch ( Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Test la m�thode saveNew
	 */
	@Test
	public void test_insert_existe_pas_table_personne() {
		try {
			Personne.deleteTable();

			Personne.createTable();

			Connection conn = DBConnection.getConnection("testpersonne") ;
			
			p1= new Personne("fincher", "steven");
			p1.save();

			java.sql.Statement stat=conn.createStatement();
			stat.execute("Select * from personne");
			ResultSet rs = stat.getResultSet();
			boolean presence = false;
			while (rs.next()) {
				String nom = rs.getString("NOM");
				if( nom.equals("fincher")) presence = true;
			}
			assertEquals("La table personne doit contenir le r�alisateur", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Test la m�thode update
	 */
	@Test
	public void test_insert_existe_table_personne() {
		try {
			Personne.deleteTable();
			Personne.createTable();

			p1= new Personne("fincher", "steven");
			p1.save();
			p1.setNom("test2");
			p1.save();
			Connection conn = DBConnection.getConnection("testpersonne") ;

			java.sql.Statement stat=conn.createStatement();
			stat.execute("Select * from personne");
			ResultSet rs = stat.getResultSet();
			boolean presence = false;
			while (rs.next()) {
				String nom = rs.getString("NOM");
				if( nom.equals("test2")) presence = true;
			}
			assertEquals("La table personne doit contenir le nom du r�alisateur modifi�", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Test la m�thode findById
	 */
	@Test
	public void test_findById_table_personne() {
		try {
			Personne.deleteTable();
			Personne.createTable();
			p1= new Personne("fincher", "steven");
			p1.save();
			
			Connection conn = DBConnection.getConnection("testpersonne") ;

			boolean presence = false;

			String nom= Personne.findById(1).getNom();
			if( nom.equals("fincher")) presence = true;

			assertEquals("La table personne doit contenir le r�alisateur � l'id voulu", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Teste la m�thode findByName
	 */
	@Test
	public void test_findByName_table_personne() {
		try {
			Personne.deleteTable();
			Personne.createTable();
			
			p1= new Personne("fincher", "steven");
			p1.save();
			Connection conn = DBConnection.getConnection("testpersonne") ;
			
			boolean presence = false;

			ArrayList<Personne> personnes= Personne.findByName(p1.getNom());
			for(int i=0;i<personnes.size();i++){
				String titre = personnes.get(i).getNom();
				if( titre.equals("fincher")) presence = true;
			}
			
			assertEquals("La table personne doit contenir le r�alisateur du nom voulu", true, presence);
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Test la m�thode findAll
	 */
	@Test
	public void test_findAll_table_personne() {
		try {
			Personne.deleteTable();
			Personne.createTable();
			
			p1= new Personne("fincher1", "steven");
			p1.save();
			
			p2 = new Personne("fincher2", "zefzefezf");
			p2.save();
			Connection conn = DBConnection.getConnection("testpersonne") ;
			
			boolean presence1 = false;
			boolean presence2 = false;
			ArrayList<Personne> personnes= Personne.findAll();
			for(int i=0;i<personnes.size();i++){
				String nom = personnes.get(i).getNom();
				System.out.println(nom);
				if( nom.equals("fincher1")) presence1 = true;
				if( nom.equals("fincher2")) presence2 = true;
			}
			assertEquals("La table personne doit contenir tous les r�alisateurs", true, (presence1 && presence2));
			Personne.deleteTable();
		} catch ( Exception e){
			e.printStackTrace();
		}
	}
}
