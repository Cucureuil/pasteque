package activeRecord ;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class DBConnection {

	private static DBConnection instance ;
	private String user, password ;
	private static String dbName = "";
	private static String urlDB;
	private static Properties prop ;
	private static Connection connect = null ;
	
	private DBConnection(String user, String pwd, String dbName) throws SQLException {
		this.user = user ;
		password = pwd ;
		String serverName = "localhost";
		//Attention, sous MAMP, le port est 8889
		String portNumber = "3306";
		// iL faut une base nommee testPersonne !
		if(dbName.equals(""))
			dbName = "testPersonne";
		else
			dbName = dbName ;

		// creation de la connection
		prop = new Properties();
		prop.put("user", user);
		prop.put("password", password);
		urlDB = "jdbc:mysql://" + serverName + ":";
		urlDB += portNumber + "/" + dbName;
		
		connect = DriverManager.getConnection(urlDB, prop);
	}
	
	public synchronized static Connection getConnection(String name) throws SQLException {
		if(instance==null) {
			instance = new DBConnection("root", "", name) ;
		}
		return connect ;
	}
	
	public synchronized static void setNomDB(String nom) throws SQLException {
		String[] url = urlDB.split("/") ;
		url[url.length-1] = nom ;
		urlDB="jdbc:mysql:/" ;
		for(int i=1 ; i<url.length ; i++) {
			urlDB+=url[i] ;
			if(i!=url.length-1)
				urlDB+="/" ;
		}
			System.out.println(urlDB);
		connect = DriverManager.getConnection(urlDB, prop);
	}
}
