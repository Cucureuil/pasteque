package activeRecord;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Classe qui mod�lise une personne
 */
public class Personne {

	/**
	 * Attributs
	 * id : int 
	 * prenom : string
	 * nom : string
	 */
	private int id;
	private String nom, prenom;

	/**
	 * Constructeur de Personne
	 * @param n String, le nom de la personne a cr�er
	 * @param p String, le pr�nom de la personne � cr�er
	 */
	public Personne(String n, String p){
		this.prenom=p;
		this.nom=n;
		this.id=-1;
	}

	/**
	 * Cr�er la table personne
	 * @throws SQLException
	 */
	public static void createTable() throws SQLException {
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String requete = "CREATE TABLE IF NOT EXISTS personne (ID int(11) NOT NULL AUTO_INCREMENT,NOM varchar(40) NOT NULL,PRENOM varchar(40) NOT NULL,PRIMARY KEY (ID)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(requete);
		System.out.println("1) creation table Personne\n");
	}

	/**
	 * Insert la personne � la table
	 * @throws SQLException
	 */
	public void save() throws SQLException {
		Connection connect = DBConnection.getConnection("testpersonne");
		//si la personne est d�j� enregistr�e on udpate, sinon on l'insert dans la table et on r�cup�re l'id
		if(this.id==-1) {
			saveNew() ;
		}
		else 
			update() ;
	}

	/**
	 * S'execute lorsque l'on tente d'ins�rer une personne qui n'existe pas encore
	 * @throws SQLException
	 */
	private void saveNew() throws SQLException {
		Connection connect = DBConnection.getConnection("testpersonne");
		String requete = "Insert into personne (nom,prenom) VALUES (?,?)";
		PreparedStatement prep;
		prep = connect.prepareStatement(requete,Statement.RETURN_GENERATED_KEYS);
		prep.setString(1,this.getNom());
		prep.setString(2,this.prenom);
		prep.executeUpdate();

		// recupere le nouvel id
		int autoInc = -1;
		ResultSet rs= prep.getGeneratedKeys();
		if(rs.next()){
			autoInc = rs.getInt(1);
		}
		this.id=autoInc;
	}

	/**
	 * Actualise les attributs de la personne
	 * @throws SQLException
	 */
	private void update() throws SQLException {
		System.out.println("update "+this.nom + " " + this.prenom+ " "+this.id);
		Connection connect = DBConnection.getConnection("testpersonne");
		String requete = "UPDATE personne SET nom=? ,  prenom=? WHERE id=?";
		PreparedStatement prep;
		prep = connect.prepareStatement(requete);
		prep.setString(1,this.nom);
		prep.setString(2,this.prenom);
		prep.setInt(3,this.id);
		Personne p = Personne.findById(this.id);
		System.out.println("affiche : "+p.getNom() + " "+p.getPrenom());
		prep.executeUpdate();
	}

	/**
	 * Trouve la personne qui a l'id en param�tre
	 * @param i l'id de la personne que l'on cherche
	 * @return Personne, la personne ayant l'id en param�tre
	 * @throws SQLException
	 */
	public static Personne findById(int i) throws SQLException {
		Personne p = null ;
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String SQLPrep = "SELECT * FROM Personne WHERE id=?;";
		PreparedStatement prep1 = conn.prepareStatement(SQLPrep);
		prep1.setInt(1, i);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		// s'il y a un resultat
		if (rs.next()) {
			p = new Personne(rs.getString("nom"), rs.getString("prenom")) ;
		}
		return p;
	}


	/**
	 * Trouve un r�alisateur � partir de son nom
	 * @param string nom � trouver
	 * @return la lsite des personnes ayant le nom voulu
	 * @throws SQLException
	 */
	public static ArrayList<Personne> findByName(String string) throws SQLException {
		ArrayList<Personne> personne = new ArrayList<Personne>() ;
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String SQLPrep = "SELECT * FROM Personne WHERE nom=?;";
		PreparedStatement prep1 = conn.prepareStatement(SQLPrep);
		prep1.setString(1, string);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		// s'il y a un resultat
		if (rs.next()) {
			Personne p = new Personne(rs.getString("nom"), rs.getString("prenom")) ;
			personne.add(p) ;
		}
		return personne;
	}

	/**
	 * Retourne l'ensemble des personnes pr�sentent dans la table 
	 * @return la liste des personens pr�sentent dans la liste
	 * @throws SQLException
	 */
	public static ArrayList<Personne> findAll() throws SQLException {
		ArrayList<Personne> personne = new ArrayList<Personne>() ;
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String SQLPrep = "SELECT * FROM Personne;";
		PreparedStatement prep1 = conn.prepareStatement(SQLPrep);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		// s'il y a un resultat
		while (rs.next()) {
			Personne p = new Personne(rs.getString("nom"), rs.getString("prenom")) ;
			personne.add(p) ;
		}
		return personne;
	}

	/**
	 * Supprime la personne
	 * @throws SQLException
	 */
	public void delete() throws SQLException {
		Connection conn = DBConnection.getConnection("testpersonne") ;
		PreparedStatement prep = conn.prepareStatement("DELETE FROM Personne WHERE id=?");
		prep.setInt(1, this.id);
		prep.execute();
		this.id = -1 ;
	}

	/**
	 * Red�finie le nom de la personne
	 * @param string nouveau nom
	 */
	public void setNom(String string) {
		if(string!=null){
			this.nom=string;
			System.out.println("actualise le nom ! "+this.nom);
		}
	}

	/**
	 * Retourne l'id de la personne
	 * @return int , id de la personne
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * red�finie le pr�nom de la personne
	 * @param string nouveau pr�nom de la personne
	 */
	public void setPrenom(String string) {
		if(string!=null){
			this.prenom=string;
		}
	}

	/**
	 * Retourne le nom de a personne
	 * @return string , le nom
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Retourne le pr�nom de la personne
	 * @return String prenom
	 */
	public String getPrenom() {
		return this.prenom;
	}

	/**
	 * Supprime la table personne
	 * @throws SQLException
	 */
	public static void deleteTable() throws SQLException {
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String requete = "DROP TABLE IF EXISTS personne" ;
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(requete);
		System.out.println("1) Supression table Personne\n");		
	}

	/**
	 * Red�finie l'id de la personne
	 * @param id int, nouvel id
	 */
	private void setId(int id) {
		if(this.id==-1) {
			this.id = id ;
		}
	}
}
