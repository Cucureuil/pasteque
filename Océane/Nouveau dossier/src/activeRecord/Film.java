package activeRecord;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Mod�lise un film
 */
public class Film {

	/**
	 * ATTRIBUTS
	 */
	private String titre ;
	private int id, id_real ;

	/**
	 * Cr�er un film en asociant un titre � un r�alisateur
	 * @param string
	 * @param p2
	 */
	public Film(String string, Personne p2) {
		this.titre = string ;
		this.id_real = p2.getId() ;
		this.id = -1 ;
	}

	/**
	 * Cr�er un film un id de film avec un titre et un id de r�alisateur
	 * @param titre
	 * @param idf
	 * @param idr
	 */
	private Film(String titre, int idf, int idr) {
		this.titre = titre ;
		this.id = idf ;
		this.id_real = idr ;
	}

	/**
	 * Cr�er la table film
	 * @throws SQLException
	 */
	public static void createTable() throws SQLException {
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String requete = "CREATE TABLE IF NOT EXISTS film (ID int(11) NOT NULL AUTO_INCREMENT,TITRE varchar(40) NOT NULL,ID_REA int(11) DEFAULT NULL,PRIMARY KEY (ID),KEY ID_REA (ID_REA)) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(requete);
		System.out.println("1) creation table Personne\n");
	}


	/**
	 * Insert un film dans la base
	 * @throws SQLException
	 * @throws RealisateurAbsentException
	 */
	public void save() throws SQLException, RealisateurAbsentException {
		Connection connect = DBConnection.getConnection("testpersonne");
		//si le r�alisateur n'existe pas on l�ve une exception
		if(this.id_real==-1) {
			throw new RealisateurAbsentException() ;
		}
		else {
			if(this.id==-1)
				saveNew() ;
			else
				update() ;
		}
	}

	/**
	 * Insert un film qui n'est pas pr�sent dans la base
	 * @throws SQLException
	 */
	private void saveNew() throws SQLException {
		Connection connect = DBConnection.getConnection("testpersonne");
		String requete = "Insert into film (titre, id_rea) VALUES (?,?)";
		PreparedStatement prep;
		prep = connect.prepareStatement(requete,Statement.RETURN_GENERATED_KEYS);
		prep.setString(1,this.titre);
		prep.setInt(2,this.id_real);
		prep.executeUpdate();

		// recupere le nouvel id
		int autoInc = -1;
		ResultSet rs= prep.getGeneratedKeys();
		if(rs.next()){
			autoInc = rs.getInt(1);
		}
		this.id=autoInc;
	}

	/**
	 * Insert un film qui est d�j� pr�sent dans la base (le met � jour)
	 * @throws SQLException
	 */
	private void update() throws SQLException {
		Connection connect = DBConnection.getConnection("testpersonne");
		String requete = "UPDATE film SET titre=? ,  id_rea=? WHERE id=?";
		PreparedStatement prep;
		prep = connect.prepareStatement(requete);
		prep.setString(1,this.titre);
		prep.setInt(2,this.id_real);
		prep.setInt(3,this.id);
		prep.executeUpdate();
	}

	/**
	 * Trouve un film via son id
	 * @param i int , l'id du film que j'on veut trouver
	 * @return Film, le film qui correspond � l'id
	 * @throws SQLException
	 */
	public static Film findById(int i) throws SQLException {
		Film f = null ;
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String SQLPrep = "SELECT * FROM film WHERE id=?;";
		PreparedStatement prep1 = conn.prepareStatement(SQLPrep);
		prep1.setInt(1, i);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		// s'il y a un resultat
		if (rs.next()) {
			f = new Film(rs.getString("titre"), rs.getInt("id"), rs.getInt("ID_REA")) ;
		}
		return f;
	}
	
	/**
	 * Retourne tous les films d'un r�alisateur
	 * @param p Personne, le r�alisateur
	 * @return Liste des films du r�alisateur
	 * @throws SQLException
	 */
	public static ArrayList<Film> findByRealisateur(Personne p) throws SQLException {
		ArrayList<Film> f = new ArrayList<Film>() ;
		int i = p.getId() ;
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String SQLPrep = "SELECT * FROM film WHERE id=?;";
		PreparedStatement prep1 = conn.prepareStatement(SQLPrep);
		prep1.setInt(1, i);
		prep1.execute();
		ResultSet rs = prep1.getResultSet();
		// tant qu'il y a des resultats
		while (rs.next()) {
			f.add(new Film(rs.getString("titre"), rs.getInt("id"), rs.getInt("ID_REA"))) ;
		}
		return f;
	}

	/**
	 * Retourne l'id du film
	 * @return int id du film
	 */
	public int getId() {
		return this.id ;
	}

	/**
	 * Retourne le titre du film
	 * @return string , le titre
	 */
	public String getTitre() {
		return this.titre ;
	}

	/**
	 * Retourne le r�alisateur du film
	 * @return Personne, le r�alisateur du film
	 * @throws SQLException
	 */
	public Personne getRealisateur() throws SQLException {
		return Personne.findById(this.id_real) ;
	}

	/**
	 * Red�finie le titre du film
	 * @param string le nouveau titre
	 */
	public void setTitre(String string) {
		this.titre = string ;
	}

	/**
	 * Supprime la table Film
	 * @throws SQLException
	 */
	public static void deleteTable() throws SQLException {
		Connection conn = DBConnection.getConnection("testpersonne") ;
		String requete = "DROP TABLE IF EXISTS film" ;
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(requete);
		System.out.println("1) Supression table Personne\n");	
	}

	/**
	 * Supprime le film de la base
	 * @throws SQLException
	 */
	public void delete() throws SQLException {
		Connection conn = DBConnection.getConnection("testpersonne") ;
		PreparedStatement prep = conn.prepareStatement("DELETE FROM film WHERE id=?");
		prep.setInt(1, this.id);
		prep.execute();
		this.id = -1 ;
	}

}
