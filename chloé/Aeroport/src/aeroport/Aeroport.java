package aeroport;

public class Aeroport{

	private boolean piste_libre;
	private static Aeroport instance;
	
	public Aeroport(){
		this.piste_libre=true;
	}
	
	public synchronized static Aeroport getInstance(){
		if(instance==null){
			instance = new Aeroport();
		}
		return instance;
	}
	
	public synchronized boolean autoriserDecollage(){
		boolean decoller = false ;
		if(piste_libre){
			decoller = true;
		}
		piste_libre=false;
		return decoller;
	}
	
	public synchronized boolean libererPiste(){
		if(autoriserDecollage()){
			this.piste_libre=true;
			return true;
		} else return false;
	}
}
