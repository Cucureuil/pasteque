package pizza;

import gestionImage.MyImage;

public class PizzaCreme implements Pizza {

	/**
	 * ATTRIBUTS
	 */
	private double prix;
	private String nomIm;
	private String description;
	
	/**
	 * CONSTRUCTEUR
	 */
	public PizzaCreme(){
		this.prix=2;
		this.nomIm="./images/fond_creme.png";
		this.description="Pizza � la cr�me ";
	}
	
	@Override
	public MyImage getPizzalm() {
		return new MyImage(this.nomIm);
	}

	@Override
	public double cout() {
		return this.prix;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

}
