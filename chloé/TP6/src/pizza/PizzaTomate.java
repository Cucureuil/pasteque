package pizza;

import gestionImage.MyImage;

public class PizzaTomate implements Pizza {

	/**
	 * ATTRIBUTS
	 */
	private double prix;
	private String nomIm;
	private String description;
	
	/**
	 * CONSTRUCTEUR
	 */
	public PizzaTomate(){
		this.prix=1.5;
		this.nomIm="./images/fond_tomate.png";
		this.description="Pizza � la Tomate ";
	}
	
	@Override
	public MyImage getPizzalm() {
		return new MyImage(this.nomIm);
	}

	@Override
	public double cout() {
		return this.prix;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

}
