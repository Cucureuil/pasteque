public class Aeroport{
	
	private boolean piste_libre;
	private static Aeroport instance ;
	
	private Aeroport(){
		this.piste_libre=true;
	}
	
	public synchronized static Aeroport getInstance() {
		if(instance==null)
			instance = new Aeroport() ;
		return instance ;
	}
	
	public synchronized boolean autoriserDecollage() {
		boolean decoller = false ;
		if(piste_libre) {
			decoller = true ;
			piste_libre = false ;
		}
		return decoller ;
	}
	
	public synchronized void libererPiste() {
		piste_libre = true ;
	}
}
