package vehicules;

public abstract class Vehicule {

	/**
	 * ATTRIBUTS
	 */
	private double vitesse;
	private double vitesseMax;
	private String type;
	
	/**
	 * Construit un v�hicule
	 * @param v double vitesse max du v�hicule
	 * @param t String type de v�hicule
	 */
	public Vehicule(double v,String t){
		this.vitesseMax=v;
		this.type=t;
	}
	
	/**
	 * GETTERS
	 */
	public double getVitesse(){
		return this.vitesse;
	}
	
	public String getType(){
		return this.type;
	}
	
	/**
	 * Augmente la vitesse du v�hicule
	 * @param a double, acceleration
	 */
	public void accelerer(double a){
		if((this.vitesse+a)<=this.vitesseMax && a>=0){
			this.vitesse+=a;
		}
	}
	
	/**
	 * Diminue la vitesse du v�hicule
	 * @param d double, deceleration
	 */
	public void decelerer(double d){
		this.vitesse-=Math.abs(d);
	}
}
