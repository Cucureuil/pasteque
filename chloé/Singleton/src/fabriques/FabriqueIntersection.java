package fabriques;

import vehicules.*;

public class FabriqueIntersection implements FabriqueVehicule{

	/**
	 * ATTRIBUTS
	 */
	private int probaVoiture,probaBus,probaByc,probaPieton;

	public FabriqueIntersection(){
		this.probaVoiture=80;
		this.probaBus=5;
		this.probaByc=5;
		this.probaPieton=10;
	}

	public FabriqueIntersection(int v, int bu, int by, int pi){

		// CALCUL DU TOTAL DES PROBA (doit �tre �gal � 100)
		int total = v+bu+by+pi;
		System.out.println(total);
		if(total==100){
			this.probaVoiture=v;
			this.probaBus=bu;
			this.probaByc=by;
			this.probaPieton=pi;
		}
	}

	/**
	 * G�n�re un v�hicule en fonction des probabilit�s qu'il soit cr��
	 */
	@Override
	public Vehicule creerVehicule() {
		// Nombre al�atoire
		int alea=(int) (Math.random()*(Math.abs(100-1)));
		// D�but de l'intervalle
		int debut_intervalle=0;
		// Fin qui est directement initialis� � celle du bus
		int fin_intervalle=(int) this.probaBus;
		// Si al�a est compris dans l'intervalle de d�but et de fin de la proba du bus alors
		// elle va cr�er un bus
		if(alea>=debut_intervalle && alea<=fin_intervalle){
			return new Bus();
		}
		// Initialise la seconde intervalle ect ..
		debut_intervalle=fin_intervalle;
		fin_intervalle=(int) (debut_intervalle+this.probaVoiture);
		if(alea>=debut_intervalle && alea<=fin_intervalle){
			return new Voiture();
		}
		debut_intervalle=fin_intervalle;
		fin_intervalle=(int) (debut_intervalle+this.probaByc);
		if(alea>=debut_intervalle && alea<=fin_intervalle){
			return new Bicyclette();
		}
		debut_intervalle=fin_intervalle;
		fin_intervalle=(int) (debut_intervalle+this.probaPieton);
		if(alea>=debut_intervalle && alea<=fin_intervalle){
			return new Pieton();
		}
		return null;
	}	
}
