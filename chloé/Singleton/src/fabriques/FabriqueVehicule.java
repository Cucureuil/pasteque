package fabriques;

import vehicules.*;

public interface FabriqueVehicule {

	public Vehicule creerVehicule();
}
