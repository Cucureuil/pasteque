package fabriques;

import vehicules.*;

public class FabriquePieton  implements FabriqueVehicule {

	public FabriquePieton(){}

	@Override
	public Vehicule creerVehicule() {
		return new Pieton();
	}
}