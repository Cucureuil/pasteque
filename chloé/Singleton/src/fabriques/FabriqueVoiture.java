package fabriques;

import vehicules.*;

public class FabriqueVoiture implements FabriqueVehicule {

	public FabriqueVoiture(){}
	
	@Override
	public Vehicule creerVehicule() {
		return new Voiture();
	}

}
