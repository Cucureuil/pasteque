package simulateur;

import java.awt.Graphics;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fabriques.FabriqueVehicule;
import vehicules.Vehicule;

public class Simulateur {

	/**
	 * ATTRIBUTS
	 */
	private FabriqueVehicule fabrique;

	/**
	 * Constructeur du simulateur
	 * @param f FabriqueIntersection
	 */
	public Simulateur(FabriqueVehicule f){
		this.fabrique=f;	
	}
	
	/**
	 * G�n�re des stats de cr�ation de v�hicule pour 100 v�hicule cr��s
	 * @return HashMap<String,int> le tableau de stat qui � chaque type associe le nombre d'entit�s cr��es
	 */
	public HashMap<String,Integer> genererStats(){
		HashMap<String,Integer> table=new HashMap<String,Integer>();
		Vehicule v;
		String key;
		table.put("Bicyclette",0);
		table.put("Bus",0);
		table.put("Voiture",0);
		table.put("Pieton",0);
		for(int i=0;i<100;i++){
			v=this.fabrique.creerVehicule();
			key=v.getType();
			System.out.println(v.getType());
			if(table.containsKey(key)){
				table.replace(key,table.get(key)+1);
			} else {
				table.put(key, 1);
			}
		}
		return table;
	}	
	
	/**
	 * M�thode d'affichage des stats de g�n�ration de v�hicule 
	 */
	public void EcrireStats(){
		HashMap<String,Integer> table=this.genererStats();
		for( String k : table.keySet()){
			System.out.println("V�hicule de type : "+k+" g�n�r� : "+table.get(k));
		}
	}
	
	public void DessinerStats(){
		JFrame f=new JFrame("Stats");
		f.setSize(200, 200);
		f.setResizable(false);
		JPanel principal=new JPanel();
		Graphics g = principal.getGraphics();
//		for(String key : this.genererStats().keySet()){}
		
		//principal.paintImmediately(g.drawRect(, y, width, height););
		
		f.add(principal);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
