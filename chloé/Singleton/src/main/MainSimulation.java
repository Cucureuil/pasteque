package main;

import fabriques.FabriqueIntersection;
import fabriques.FabriqueVehicule;
import simulateur.Simulateur;

public class MainSimulation {

	public static void main(String[] args) {
		FabriqueVehicule fabrique1=new FabriqueIntersection();
		FabriqueVehicule fabrique2=new FabriqueIntersection(0,60,30,10);
		Simulateur s=new Simulateur(fabrique2);
		s.EcrireStats();
	}
}
