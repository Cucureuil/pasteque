CREATE TABLE article(
titre VARCHAR2(500) PRIMARY KEY NOT NULL,
resume VARCHAR2(500),
typearticle VARCHAR2(500)) ;

CREATE TABLE chercheur(
email VARCHAR2(500) PRIMARY KEY NOT NULL,
nomChercheur VARCHAR2(500),
prenomChercheur VARCHAR2(500),
URLChercheur VARCHAR2(500)) ;

CREATE TABLE laboratoire(
nomlabo VARCHAR2(500) PRIMARY KEY NOT NULL,
siglelabo VARCHAR2(500),
adresselabo VARCHAR2(500),
urllabo VARCHAR2(500)) ;

CREATE TABLE support(
nomSupport VARCHAR2(500) PRIMARY KEY NOT NULL,
typesupport VARCHAR2(500)) ;

CREATE TABLE annotation(
libelle VARCHAR2(500) PRIMARY KEY NOT NULL) ;

--2 foreign key
CREATE TABLE ecrire(
email VARCHAR2(500),
titre VARCHAR2(500),
PRIMARY KEY(email, titre)) ;

ALTER TABLE ecrire
 ADD CONSTRAINT FK_ECRIRE_email FOREIGN KEY(email)
 REFERENCES chercheur(email);
ALTER TABLE ecrire
 ADD CONSTRAINT FK_ECRIRE_titre FOREIGN KEY(titre)
 REFERENCES article(titre);

--titre+nomsupport foreign key
CREATE TABLE publier(
titre VARCHAR2(500),
nomsupport VARCHAR2(500),
anneepub NUMBER(38),
PRIMARY KEY(titre, nomSupport)) ;

ALTER TABLE publier
 ADD CONSTRAINT FK_PUBLIER_titre FOREIGN KEY(titre)
 REFERENCES article(titre);
ALTER TABLE publier
 ADD CONSTRAINT FK_PUBLIER_nomsupport FOREIGN KEY(nomsupport)
 REFERENCES support(nomsupport);


--2 foreign key
CREATE TABLE travailler(
email VARCHAR2(500),
nomlabo VARCHAR2(500),
PRIMARY KEY(email, nomLabo)) ;

ALTER TABLE travailler
 ADD CONSTRAINT FK_travailler_email FOREIGN KEY(email)
 REFERENCES chercheur(email);
ALTER TABLE travailler
 ADD CONSTRAINT FK_TRAVAILLER_nomlabo FOREIGN KEY(nomlabo)
 REFERENCES laboratoire(nomlabo);

--3 foreign key
CREATE TABLE annoter(
email VARCHAR2(500),
titre VARCHAR2(500),
libelle VARCHAR2(500),
PRIMARY KEY(email, titre, libelle)) ;

ALTER TABLE annoter
 ADD CONSTRAINT FK_annoter_email FOREIGN KEY(email)
 REFERENCES chercheur(email);
ALTER TABLE annoter
 ADD CONSTRAINT FK_ANNOTER_titre FOREIGN KEY(titre)
 REFERENCES article(titre);
ALTER TABLE annoter
 ADD CONSTRAINT FK_ANNOTER_libelle FOREIGN KEY(libelle)
 REFERENCES annotation(libelle);


--2 foreign key (email+titre)
CREATE TABLE noter(
email VARCHAR2(500),
titre VARCHAR2(500),
note NUMBER(38),
PRIMARY KEY(email, titre)) ;

ALTER TABLE noter
 ADD CONSTRAINT FK_noter_email FOREIGN KEY(email)
 REFERENCES chercheur(email);
ALTER TABLE noter
 ADD CONSTRAINT FK_noter_titre FOREIGN KEY(titre)
 REFERENCES article(titre);

INSERT INTO article VALUES('A User-centric Framework for Accessing Biological Sources and Tools', 'We study the representation and querying of XML with incomplete information. We consider a simple
model for XML data and their DTDs, a very simple query language, and a representation system for
incomplete information in the spirit of the representations systems developed by Imielinski and Lipski
for relational databases.', 'Long') ;
INSERT INTO article VALUES('Adding Structure to Unstructured Data', 'We develop a new schema for unstructured data. Traditional schemas resemble the
type systems of programming languages.', 'Long') ;
INSERT INTO article VALUES('PDiffView: Viewing the Difference in Provenance of Workflow Results', 'Provenance Difference Viewer (PDiffView) is a prototype based on these algorithms for differencing
runs of SPFL specifications.', 'Court') ;
INSERT INTO article VALUES('Automata and Logics for Words and Trees over an Infinite Alphabet', 'In a data word or a data tree each position carries a label from a finite alphabet and a data value
from some infinite domain. These models have been considered in the realm of semistructured data,
timed automata and extended temporal logics.
This paper survey several know results on automata and logics manipulating data words and data trees,
the focus being on their relative expressive power and decidability.', 'Long') ;
INSERT INTO article VALUES('Representing and Querying XML with Incomplete Information', 'We study the representation and querying of XML with incomplete information. We consider a simple
model for XML data and their DTDs, a very simple query language, and a representation system for
incomplete information in the spirit of the representations systems developed by Imielinski and Lipski
for relational databases. In the scenario we consider, the incomplete information about an XML
document is continuously enriched by successive queries to the document.', 'Long') ;
INSERT INTO article VALUES('The TLA + Proof System: Building a Heterogeneous Verification Platform', 'Model checking has proved to be an efficient technique for finding subtle bugs in concurrent and
distributed algorithms and systems. However, it is usually limited to the analysis of small instances
of such systems, due to the problem of state space explosion. When model checking finds no more
errors, one can attempt to verify the correctness of a model using theorem proving, which also
requires efficient tool support. ', 'Long') ;
INSERT INTO article VALUES('Partial reversal acyclicity', 'Partial Reversal (PR) is a link reversal algorithm which ensures that an initially directed acyclic
graph (DAG) is eventually a destination-oriented DAG. While proofs exist to establish the acyclicity
property of PR, they rely on assigning labels to either the nodes or the edges in the graph. In this
work we show that such labeling is not necessary and outline a simpler direct proof of the acyclicity
property.', 'Court') ;
INSERT INTO article VALUES('Reliably Detecting Connectivity Using Local Graph Traits','This paper studies local graph traits and their relationship with global graph properties.
Specifically, we focus on graph k-connectivity. First we prove a negative result that shows there does
not exist a local graph trait which perfectly captures graph k-connectivity. We then present three
different local graph traits which can be used to reliably predict the k-connectivity of a graph with
varying degrees of accuracy.','Long');
INSERT INTO article VALUES('Generalized Universality','This paper presents, two decades after k-set consensus was introduced, the generalization with k > 1
of state machine replication. We show that with k-set consensus, any number of processes can emulate k
state machines of which at least one remains highly available. While doing so, we also generalize the
very notion of consensus universality.','LONG');
INSERT INTO article VALUES(  'Transactional Memory: Glimmer of a Theory' ,  'Transactional memory (TM) is a promising paradigm for concurrent programming. This paper is an
overview of our recent theoretical work on defining a theory of TM. We first recall some TM
correctness properties and then overview results on the inherent power and limitations of TMs.' ,  'Tutoriel'
);
INSERT INTO chercheur VALUES( 'peter@cis.upenn.edu' ,     'Buneman' , 'Peter' ,  'http://homepages.inf.ed.ac.uk/opb/');
INSERT INTO chercheur VALUES(  'cohen@lri.fr' ,  'Cohen-Boulakia' , 'Sarah' ,   'http://www.lri.fr/~cohen');
INSERT INTO chercheur VALUES( 'chris@lri.fr' ,        'Froidevaux' ,    'Christine',  'http://www.lri.fr/~chris/');
INSERT INTO chercheur VALUES(   'susan@cis.upenn.edu' ,  'Davidson' , 'Susan',  'http://www.cis.upenn.edu/~susan/');
INSERT INTO chercheur VALUES(  'luc.segoufin@inria.fr' ,   'Segoufin' , 'Luc' ,   'http://www-rocq.inria.fr/~segoufin/') ;
INSERT INTO chercheur VALUES(    'lamport@microsoft.com' ,       'Lamport' , 'Leslie' ,       'http://www.lamport.org/');
INSERT INTO chercheur VALUES(  'lynch@theory.csail.mit.edu' , 'Lynch' , 'Nancy' , 'http://people.csail.mit.edu/lynch/');
INSERT INTO chercheur VALUES( 'Rachid.Guerraoui@epfl.ch' , 'Guerraoui' , 'Rachid' , 'http://lpdwww.epfl.ch/rachid/');

INSERT INTO laboratoire VALUES( 'Laboratory for Foundations of Computer Science' ,'LFCS' ,   'LFCS, School of Informatics Crichton Stree Edinburgh EH8 9LE' , '' );
INSERT INTO laboratoire VALUES( 'Department of Computer and Information Science University of Pennsylvania' , 'CIS' , '305 Levine/572 Levine North Department of Computer and Information Science University of
Pennsylvania Levine Hall 3330 Walnut Street Philadelphia, PA 19104-6389', '');
INSERT INTO laboratoire VALUES( 'Laboratoire de Recherche en Informatique' , 'LRI' , 'Bât 490 Université Paris-Sud 11 91405 Orsay Cedex France' , '');
INSERT INTO laboratoire VALUES('Laboratoire Spécification et Vérification' , 'LSV' ,'ENS de Cachan, 61 avenue du Président Wilson, 94235 CACHAN Cedex, FRANCE', '');
INSERT INTO laboratoire VALUES( 'Distributed Programming Laboratory','LPD','Bat INR 326 Station 14 1015 Lausanne Switzerland','http://lpd.epfl.ch/site/');
INSERT INTO laboratoire VALUES(  'Theory of Distributed Systems','TDS','32 Vassar Street (32-G672A) Cambridge, MA 02139, USA','http://groups.csail.mit.edu/tds/');
INSERT INTO laboratoire VALUES('Microsoft Corporation', 'Microsoft','1065 La Avenida Mountain View, CA 94043USA.','http://research.microsoft.com/');
INSERT INTO laboratoire VALUES('INRIA Saclay - Ile-de-France','INRIA Saclay','Domaine de Voluceau Rocquencourt - BP 105 78153 Le Chesnay Cedex, France','http://www.inria.fr/centre/saclay');

INSERT INTO support VALUES('ICDT', 'Conference') ;
INSERT INTO support VALUES('DILS', 'Conference') ;
INSERT INTO support VALUES('TODS', 'Journal') ;
INSERT INTO support VALUES('VLDB', 'Journal') ;
INSERT INTO support VALUES('CLS', 'Conference') ;
INSERT INTO support VALUES('CAV', 'Conference') ;
INSERT INTO support VALUES('CONCUR', 'Conference') ;
INSERT INTO support VALUES('OPODIS', 'Conference') ;
INSERT INTO support VALUES('PODC',  'Conference') ;
INSERT INTO support VALUES('ICTAC',  'Conference') ;

INSERT INTO annotation VALUES('data') ;
INSERT INTO annotation VALUES('bio') ;
INSERT INTO annotation VALUES('workflow') ;
INSERT INTO annotation VALUES('theory') ;
INSERT INTO annotation VALUES('XML') ;
INSERT INTO annotation VALUES('Concurrency') ;
INSERT INTO annotation VALUES('TLA') ;
INSERT INTO annotation VALUES('Consencus') ;
INSERT INTO annotation VALUES('Graph') ;
INSERT INTO annotation VALUES('Reliability') ;

INSERT INTO ecrire VALUES('peter@cis.upenn.edu', 'Adding Structure to Unstructured Data') ;
INSERT INTO ecrire VALUES('susan@cis.upenn.edu', 'Adding Structure to Unstructured Data') ;
INSERT INTO ecrire VALUES('susan@cis.upenn.edu', 'A User-centric Framework for Accessing Biological Sources and Tools') ;
INSERT INTO ecrire VALUES('cohen@lri.fr', 'A User-centric Framework for Accessing Biological Sources and Tools') ;
INSERT INTO ecrire VALUES('chris@lri.fr', 'A User-centric Framework for Accessing Biological Sources and Tools') ;
INSERT INTO ecrire VALUES('luc.segoufin@inria.fr', 'Automata and Logics for Words and Trees over an Infinite Alphabet') ;
INSERT INTO ecrire VALUES('luc.segoufin@inria.fr', 'Representing and Querying XML with Incomplete Information') ;
INSERT INTO ecrire VALUES('Rachid.Guerraoui@epfl.ch', 'Generalized Universality') ;
INSERT INTO ecrire VALUES('Rachid.Guerraoui@epfl.ch', 'Transactional Memory: Glimmer of a Theory') ;
INSERT INTO ecrire VALUES('lynch@theory.csail.mit.edu', 'Reliably Detecting Connectivity Using Local Graph Traits') ;
INSERT INTO ecrire VALUES('lynch@theory.csail.mit.edu', 'Partial reversal acyclicity') ;
INSERT INTO ecrire VALUES('lamport@microsoft.com', 'The TLA + Proof System: Building a Heterogeneous Verification Platform') ;

INSERT INTO publier VALUES('Adding Structure to Unstructured Data','ICDT', 1997) ;
INSERT INTO publier VALUES('A User-centric Framework for Accessing Biological Sources and Tools', 'DILS', 2005) ;
INSERT INTO publier VALUES('Representing and Querying XML with Incomplete Information', 'TODS', 2006) ;
INSERT INTO publier VALUES('PDiffView: Viewing the Difference in Provenance of Workflow Results', 'VLDB', 2009) ;
INSERT INTO publier VALUES('Automata and Logics for Words and Trees over an Infinite Alphabet', 'CLS', 2006) ;
INSERT INTO publier VALUES('The TLA + Proof System: Building a Heterogeneous Verification Platform', 'ICTAC', 2009) ;
INSERT INTO publier VALUES('Partial reversal acyclicity', 'PODC', 2001) ;
INSERT INTO publier VALUES('Reliably Detecting Connectivity Using Local Graph Traits', 'OPODIS', 2010) ;
INSERT INTO publier VALUES('Generalized Universality', 'CONCUR', 2011) ;
INSERT INTO publier VALUES('Transactional Memory: Glimmer of a Theory', 'CAV', 2010) ;

INSERT INTO travailler VALUES('peter@cis.upenn.edu', 'Laboratory for Foundations of Computer Science') ;
INSERT INTO travailler VALUES('susan@cis.upenn.edu', 'Department of Computer and Information Science University of Pennsylvania') ;
INSERT INTO travailler VALUES('peter@cis.upenn.edu', 'Department of Computer and Information Science University of Pennsylvania') ;
INSERT INTO travailler VALUES('cohen@lri.fr', 'Laboratoire de Recherche en Informatique') ;
INSERT INTO travailler VALUES('chris@lri.fr', 'Laboratoire de Recherche en Informatique') ;
INSERT INTO travailler VALUES('luc.segoufin@inria.fr', 'Laboratoire Spécification et Vérification') ;
INSERT INTO travailler VALUES('luc.segoufin@inria.fr', 'INRIA Saclay - Ile-de-France') ;
INSERT INTO travailler VALUES('lamport@microsoft.com', 'Microsoft Corporation') ;
INSERT INTO travailler VALUES('lynch@theory.csail.mit.edu', 'Theory of Distributed Systems');
INSERT INTO travailler VALUES('Rachid.Guerraoui@epfl.ch', 'Distributed Programming Laboratory') ;
INSERT INTO travailler VALUES('Rachid.Guerraoui@epfl.ch', 'INRIA Saclay - Ile-de-France') ;

INSERT INTO annoter VALUES('luc.segoufin@inria.fr', 'Adding Structure to Unstructured Data', 'data') ;
INSERT INTO annoter VALUES('peter@cis.upenn.edu', 'A User-centric Framework for Accessing Biological Sources and Tools', 'bio') ;
INSERT INTO annoter VALUES('peter@cis.upenn.edu', 'Adding Structure to Unstructured Data', 'XML') ;
INSERT INTO annoter VALUES('peter@cis.upenn.edu', 'PDiffView: Viewing the Difference in Provenance of Workflow Results', 'workflow') ;
INSERT INTO annoter VALUES('cohen@lri.fr', 'Automata and Logics for Words and Trees over an Infinite Alphabet', 'theory') ;
INSERT INTO annoter VALUES('lamport@microsoft.com', 'The TLA + Proof System: Building a Heterogeneous Verification Platform', 'TLA') ;
INSERT INTO annoter VALUES('lynch@theory.csail.mit.edu', 'Transactional Memory: Glimmer of a Theory', 'Concurrency') ;
INSERT INTO annoter VALUES('Rachid.Guerraoui@epfl.ch', 'Partial reversal acyclicity', 'Graph' ) ;
INSERT INTO annoter VALUES('Rachid.Guerraoui@epfl.ch', 'Reliably Detecting Connectivity Using Local Graph Traits', 'Reliability') ;

INSERT INTO noter VALUES('luc.segoufin@inria.fr', 'Adding Structure to Unstructured Data', 4) ;
INSERT INTO noter VALUES('luc.segoufin@inria.fr', 'Automata and Logics for Words and Trees over an Infinite Alphabet', 1) ;
INSERT INTO noter VALUES('luc.segoufin@inria.fr', 'A User-centric Framework for Accessing Biological Sources and Tools', 4) ;
INSERT INTO noter VALUES('luc.segoufin@inria.fr', 'PDiffView: Viewing the Difference in Provenance of Workflow Results', 5) ;
INSERT INTO noter VALUES('luc.segoufin@inria.fr', 'Representing and Querying XML with Incomplete Information', 1 ) ;
INSERT INTO noter VALUES('peter@cis.upenn.edu', 'A User-centric Framework for Accessing Biological Sources and Tools', 2 ) ;
INSERT INTO noter VALUES('peter@cis.upenn.edu', 'Automata and Logics for Words and Trees over an Infinite Alphabet', 1) ;
INSERT INTO noter VALUES('cohen@lri.fr', 'A User-centric Framework for Accessing Biological Sources and Tools', 2 ) ;
INSERT INTO noter VALUES('cohen@lri.fr', 'PDiffView: Viewing the Difference in Provenance of Workflow Results', 1) ;
INSERT INTO noter VALUES('Rachid.Guerraoui@epfl.ch', 'Adding Structure to Unstructured Data', 1) ;
INSERT INTO noter VALUES('Rachid.Guerraoui@epfl.ch' , 'Automata and Logics for Words and Trees over an Infinite Alphabet', 4) ;
INSERT INTO noter VALUES( 'Rachid.Guerraoui@epfl.ch', 'A User-centric Framework for Accessing Biological Sources and Tools', 2) ;
INSERT INTO noter VALUES( 'Rachid.Guerraoui@epfl.ch', 'PDiffView: Viewing the Difference in Provenance of Workflow Results', 1) ;
INSERT INTO noter VALUES('Rachid.Guerraoui@epfl.ch', 'Representing and Querying XML with Incomplete Information', 5) ;
INSERT INTO noter VALUES('lamport@microsoft.com', 'A User-centric Framework for Accessing Biological Sources and Tools', 3) ;
INSERT INTO noter VALUES('lamport@microsoft.com', 'Automata and Logics for Words and Trees over an Infinite Alphabet', 4) ;

--TRIGGER 8.1
CREATE OR REPLACE TRIGGER noteParCoauteur BEFORE INSERT ON noter
FOR EACH ROW
  DECLARE
   CURSOR c_auteur IS
    SELECT email FROM ecrire WHERE titre=:new.titre ;
  BEGIN
    FOR rec_auteur IN c_auteur LOOP
       IF(rec_auteur.email=:new.email) THEN
        Raise_Application_Error(-20001, 'C est un co auteur qui note l article') ;
      END IF ;
    END LOOP ;
  END ;

--TRIGGER 8.2
CREATE TABLE log_chercheur (
utilisateur VARCHAR(200) PRIMARY KEY NOT NULL ,
dateMAJ DATE,
typeAction VARCHAR(200)) ;

CREATE OR REPLACE TRIGGER sauvActions AFTER INSERT OR UPDATE ON annoter
  FOR EACH ROW
  BEGIN
  IF(INSERTING) THEN
    INSERT INTO log_chercheur VALUES(USER, SYSDATE, 'insert') ;
    ELSE
    INSERT INTO log_chercheur VALUES(USER, SYSDATE, 'update') ;
  END IF;
END;
