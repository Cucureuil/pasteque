package programme;

import java.sql.*;
import java.io.*;
import java.util.*;

public class appli_JDBC {

	public static void main(String[] args) {
		try {
			appli_JDBC app = new appli_JDBC();
			Scanner sc = new Scanner(System.in);
			/*
			 * String util="masoni4u"; String mdp="MotDePassePro";
			 * //Class.forName("oracle.jdbc.driver.OracleDriver"); String
			 * urlDB="jdbc:oracle:thin:@charlemagne:1521:infodb"; Connection
			 * conn= DriverManager.getConnection(urlDB,util,mdp);
			 */

			// A VIRER
			String serverName = "localhost";
			// Attention, sous MAMP, le port est 8889
			String portNumber = "3306";

			// iL faut une base nommee testPersonne !
			String dbName = "projet_bdd_s3";

			// creation de la connection
			Properties prop = new Properties();
			prop.put("user", "root");
			prop.put("password", "");
			String urlDB = "jdbc:mysql://" + serverName + ":";
			urlDB += portNumber + "/" + dbName;
			Connection conn = DriverManager.getConnection(urlDB, prop);
			// JUSQUE LA

			conn.setAutoCommit(false); // ou true faut voir

			// menu à faire bien
			String auteur = sc.nextLine();
			System.out.println(app.listeCoAuteurs(conn, auteur));
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception l) {
			l.printStackTrace();
		}
	}

	/**
	 * Méthode qui permet de déterminer la liste des articles écrits par un
	 * auteur donné
	 * 
	 * @param conn
	 *            Connection courante
	 * @param auteur
	 *            String qui correspond à l'adresse mail de l'auteur (car c'est
	 *            la clé principale de la table auteur)
	 */
	public String listeArticles(Connection conn, String auteur) throws Exception {
		String retour = "";
		String req = "SELECT titre from ecrire where email=?";
		PreparedStatement stmt2 = conn.prepareStatement(req);
		stmt2.setString(1, auteur);
		ResultSet res = stmt2.executeQuery();
		// le if permet de différencier l'affichage en fonction de si l'auteur
		// existe (ou a écrit des articles) ou non
		if (res.next()) {
			retour += "Voici les articles que " + auteur + " a écrits : \n";
			do {
				retour += res.getString("titre") + "\n";
			} while (res.next());
		} else
			retour += "Cet auteur n'a écrit aucun article ou il n'existe pas \n";

		res.close();

		return retour;
	}

	/**
	 * Méthode qui permet d'afficher la liste des co-auteurs ayant travaillé
	 * avec un chercheur donné
	 * 
	 * @param conn
	 *            Connection courante
	 * @param auteur
	 *            String qui correspond à l'adresse mail de l'auteur (car c'est
	 *            la clé principale de la table auteur)
	 */
	public String listeCoAuteurs(Connection conn, String auteur) throws Exception {
		String retour = "";
		// on trouve tous les titres de l'auteur
		String req = "SELECT titre from ecrire where email=?";
		PreparedStatement stmtTitre = conn.prepareStatement(req);
		stmtTitre.setString(1, auteur);
		ResultSet resTitre = stmtTitre.executeQuery();

		// pour chaque titre on cherche les co auteurs qui ont travaillé dessus
		ResultSet resAuteurs = null;
		PreparedStatement stmtAuteurs = null;
		boolean auteurs = true;
		boolean debut = true;
		if (resTitre.next()) {
			do {
				req = "SELECT email from ecrire where titre=? and email!=?";
				stmtAuteurs = conn.prepareStatement(req);
				stmtAuteurs.setString(1, resTitre.getString("titre"));
				stmtAuteurs.setString(2, auteur);
				resAuteurs = stmtAuteurs.executeQuery();
				if (resAuteurs.next()) {
					if (debut) {
						retour += "Cet auteur a travaillé avec : \n";
						debut = false;
					}
					do {
						retour += resAuteurs.getString("email") + "\n";
					} while (resAuteurs.next());
				} else
					retour += "Cet auteur travaille seul \n";
			} while (resTitre.next());
		} else {
			retour += "Cet auteur n'existe pas. \n";
			auteurs = false;
		}

		stmtTitre.close();
		resTitre.close();
		if (auteurs) {
			resAuteurs.close();
			stmtAuteurs.close();
		}

		return retour;

	}

	/**
	 * Méthode qui permet de trouver la liste des laboratoires dans lesquels
	 * l'auteur travaille
	 * 
	 * @param conn
	 *            Connection courante
	 * @param auteur
	 *            String qui correspond à l'adresse mail de l'auteur (car c'est
	 *            la clé principale de la table auteur)
	 */
	public String listeLabo(Connection conn, String auteur) throws Exception {
		String retour = "";
		String req = "SELECT nomlabo from travailler where email=?";
		PreparedStatement stmt2 = conn.prepareStatement(req);
		stmt2.setString(1, auteur);
		ResultSet res = stmt2.executeQuery();
		if (res.next()) {
			retour += "Cet auteur travaille dans les laboratoires suivants : \n";
			do {
				retour += res.getString("nomLabo") + "\n";
			} while (res.next());
		} else
			retour += "Cet auteur n'existe pas. \n";

		stmt2.close();

		return retour;

	}

	/**
	 * Méthode qui affiche la liste des chercheurs ayant annoté au moins un
	 * nombre donné d'articles
	 * 
	 * @param conn
	 *            Connection courante
	 * @param nb
	 *            nombre d'articles que les auteurs doivent avoir noté
	 */
	public String nombreAnnotations(Connection conn, int nb) throws Exception {
		String retour = "";
		Statement stmt = conn.createStatement();
		String req = "SELECT Count(*) as c, email from noter group by email";
		ResultSet res = stmt.executeQuery(req);
		boolean trouve = false;
		while (res.next()) {
			if (res.getInt("c") >= nb) {
				if (!trouve) // permet d'afficher cette ligne seulement la 1ère
								// fois
					retour += "Les auteurs qui ont écrit plus de " + nb + " articles sont : \n";
				retour += res.getString("email") + "\n";
				trouve = true;
			}
		}
		if (!trouve)
			retour += "Aucun auteur n'a écrit plus de " + nb + " articles \n";

		stmt.close();
		res.close();

		return retour;

	}

	/**
	 * Méthode qui permet de calculer la moyenne des notes données par un
	 * chercheur donné
	 * 
	 * @param conn
	 *            Connection courante
	 * @param auteur
	 *            String qui correspond à l'adresse mail de l'auteur (car c'est
	 *            la clé principale de la table auteur)
	 */
	public String moyenneNotes(Connection conn, String auteur) throws Exception {
		String retour = "";
		String req = "SELECT Avg(note) as moy from noter where email=?";
		// verifAuteurNote permet de savoir si l'auteur est dans la table noter
		String verifAuteurNote = "Select * from noter where email=?";
		PreparedStatement stmtAuteur = conn.prepareStatement(verifAuteurNote);
		PreparedStatement stmtMoy = conn.prepareStatement(req);
		stmtAuteur.setString(1, auteur);
		stmtMoy.setString(1, auteur);
		ResultSet resAuteur = stmtAuteur.executeQuery();
		ResultSet resMoy = stmtMoy.executeQuery();
		// si l'auteur est dans la table note et qu'il existe on cherche la
		// moyenne de ses notes
		if (resAuteur.next()) {
			resMoy.next();
			retour += "L'auteur " + auteur + " a donné en moyenne une note de " + resMoy.getInt("moy")
					+ " aux articles qu'il a lus \n";
		} else
			retour += "Cet auteur n'existe pas ou il n'a noté aucun article. \n";

		stmtAuteur.close();
		stmtMoy.close();
		resAuteur.close();
		resMoy.close();

		return retour;

	}

	/**
	 * Méthode qui permet de déterminer d'afficher les informations des
	 * chercheurs d
	 * 
	 * @param conn
	 *            Connection courante
	 * @param auteur
	 *            String qui correspond à l'auteur dont on veut les
	 *            informations (cette méthodes est appelée par infosLabo())
	 */
	public String infosChercheur(Connection conn, String auteur) throws Exception {
		String retour = "";
		// Affichage du nombre d'articles
		String count = "SELECT Count(*) AS c FROM ecrire WHERE email=?";
		String verif = "Select * from ecrire where email=?";
		PreparedStatement stmtCount = conn.prepareStatement(count);
		PreparedStatement stmtVerif = conn.prepareStatement(verif);
		stmtCount.setString(1, auteur);
		stmtVerif.setString(1, auteur);
		ResultSet resCount = stmtCount.executeQuery();
		ResultSet resVerif = stmtVerif.executeQuery();
		if (resVerif.next()) {
			resCount.next();
			retour += "L'auteur " + auteur + " a écrit " + resCount.getInt("c") + " articles \n";
		} else
			retour += "Cet auteur n'existe pas ou il n'a écrit aucun article. \n";

		// Affichage du nombre de notes
		count = "SELECT Count(*) AS c FROM noter WHERE email=?";
		verif = "Select * from noter where email=?";
		stmtCount = conn.prepareStatement(count);
		stmtVerif = conn.prepareStatement(verif);
		stmtCount.setString(1, auteur);
		stmtVerif.setString(1, auteur);
		resCount = stmtCount.executeQuery();
		resVerif = stmtVerif.executeQuery();
		if (resVerif.next()) {
			resCount.next();
			retour += "L'auteur " + auteur + " a noté " + resCount.getInt("c") + " articles \n";
		} else
			retour += "Cet auteur n'existe pas ou il n'a noté aucun article. \n";

		// Affichage de la moyenne des notes
		retour += moyenneNotes(conn, auteur);

		stmtCount.close();
		stmtVerif.close();
		resCount.close();
		resVerif.close();

		return retour;

	}

	/**
	 * Méthode qui permet de déterminer d'afficher les informations des
	 * chercheurs d'un laboratoire donné
	 * 
	 * @param conn
	 *            Connection courante
	 * @param labo
	 *            String qui correspond au laboratoire dont on veut récupérer
	 *            les informations sur les chercheurs
	 */
	public String infosLabo(Connection conn, String labo) throws Exception {
		String retour = "";
		String verif = "Select * from travailler where nomLabo=?";
		PreparedStatement stmtVerif = conn.prepareStatement(verif);
		stmtVerif.setString(1, labo);
		ResultSet resVerif = stmtVerif.executeQuery();
		if (resVerif.next()) {
			do {
				retour += infosChercheur(conn, resVerif.getString("email"));
			} while (resVerif.next());
		} else {
			retour += "Ce laboratoire n'existe pas ou aucun chercheur n'y travaille \n";
		}

		stmtVerif.close();
		resVerif.close();

		return retour;

	}

	/**
	 * Méthode qui permet de vérifier que la note maximale donnée à un
	 * article ne l'a pas été par un chercheur du même laboratoire que
	 * l'auteur
	 * 
	 * @param conn
	 *            Connection courante
	 * @param auteur
	 *            String qui correspond à l'adresse mail de l'auteur (car c'est
	 *            la clé principale de la table auteur)
	 */
	public String noteMax(Connection conn, String article) throws Exception {
		String retour = "";
		// Sélection du (ou des) chercheur(s) qui a donné la note maximale à
		// l'article
		String cherchNote = "SELECT email FROM noter WHERE titre = ? AND note >=ALL(SELECT Max(note) FROM noter WHERE titre =  ? )";
		PreparedStatement stmtNote = conn.prepareStatement(cherchNote);
		stmtNote.setString(1, article);
		stmtNote.setString(2, article);
		ResultSet resNote = stmtNote.executeQuery();

		// Création des requêtes pour trouver les labos des chercheurs
		String auteur = "SELECT nomLabo from travailler natural join ecrire where titre = ?";
		String chercheur = "SELECT nomLabo from travailler where email = ?";
		PreparedStatement stmtAuteur = conn.prepareStatement(auteur);
		PreparedStatement stmtChercheur = conn.prepareStatement(chercheur);
		stmtAuteur.setString(1, article);
		ResultSet resAuteur = stmtAuteur.executeQuery();
		ResultSet resChercheur = null;

		boolean labos = false;
		int t = 0;
		boolean nonNote = false;
		// on trouve les labos des auteurs qui ont écrit l'article
		if (resAuteur.next()) {
			do {
				// on trouve les mails des auteurs qui ont noté l'article
				if (resNote.next()) {
					do {
						stmtChercheur.setString(1, resNote.getString(1)); // on
																			// récupère
																			// le
																			// labo
																			// du
																			// chercheur
						resChercheur = stmtChercheur.executeQuery();

						// on vérifie que les 2 labos sont différents
						while (resChercheur.next()) {
							if (resChercheur.getString(1).equals(resAuteur.getString(1)))
								labos = true;
						}
					} while (resNote.next());
				} else {
					// Permet de n'afficher ce message qu'une fois
					if (t == 0) {
						retour += "Cet article n'a pas été noté \n";
						nonNote = true;
					}
				}
				t++;
			} while (resAuteur.next());
		} else {
			nonNote = true;
			retour += "Cet article n'existe pas \n";
		}

		if (!nonNote) {
			if (labos) {
				retour += "La meilleure note de l'article a été donnée par un chercheur du même labo qu'un des auteurs \n";
			} else
				retour += "La meilleure note de l'article n'a pas été donnée par un chercheur du même labo qu'un des auteurs \n";
			stmtNote.close();
			resChercheur.close();
		}

		resNote.close();
		stmtAuteur.close();
		stmtChercheur.close();
		resAuteur.close();

		return retour;

	}
}