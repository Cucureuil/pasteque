package vues;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controleurs.ControleBouton;
import modeles.Modele;

public class VueGestionAction extends JPanel {

	private final String[] liste_programme = { "Programme 1", "Programme 2", "Programme 3", "Programme 4",
			"Programme 5", "Programme 6", "Programme 7" };
	protected Modele mod;

	public VueGestionAction(Modele m) {
		this.mod = m;

		this.setPreferredSize(new Dimension(125, 50));
		// this.setBackground(Color.GREEN);

		// SELECTION DU PROGRAMME A EXECUTER
		JPanel panel_selection = new JPanel();
		JLabel label1 = new JLabel("Choix du programme : ");
		JComboBox box_programmes = new JComboBox(liste_programme);

		// CONTROLEUR JCOMBO BOX
		box_programmes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mod.setProgrammeCourant(box_programmes.getSelectedIndex() + 1);
			}
		});
		// AJOUT
		panel_selection.setLayout(new BorderLayout());
		panel_selection.add(label1, BorderLayout.NORTH);
		panel_selection.add(box_programmes, BorderLayout.SOUTH);

		// SAISIE DU TEXTE SI LE PROGRAMME EN A BESOIN
		JPanel panel_saisie = new JPanel();
		panel_saisie.setPreferredSize(new Dimension(200, 60));
		JLabel label2 = new JLabel("Saisie du param�tre :");
		JTextField saisieParam = new JTextField();
		saisieParam.setPreferredSize(new Dimension(100, 30));
		// Ajouts
		panel_saisie.add(label2, BorderLayout.NORTH);
		panel_saisie.add(saisieParam, BorderLayout.SOUTH);

		// CONTROLEUR TEXTE
		saisieParam.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// ajouterParametre(saisieParam.getText());
				// saisieParam.setText("");
				// repaint();
			}
		});

		// BOUTON D'EXECUTION DU PROGRAMME
		JPanel panel_bouton = new JPanel();
		panel_bouton.setPreferredSize(new Dimension(120, 40));

		JButton executer = new JButton("Executer");
		executer.setPreferredSize(new Dimension(100, 30));
		panel_bouton.add(executer, BorderLayout.SOUTH);

		// CONTROLEUR BOUTON
		ControleBouton cb = new ControleBouton(this.mod);
		executer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String p = saisieParam.getText();
				ajouterParametre(saisieParam.getText());
				saisieParam.setText("");
				try {
					mod.methodeAexecuter();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				repaint();
			}
		});

		// DESCRIPTION EVENTUELLE

		this.add(panel_selection);
		this.add(panel_saisie);
		this.add(panel_bouton);
	}

	private void ajouterParametre(String p) {
		this.mod.setParametre(p);
	}
}
