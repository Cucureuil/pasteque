package vues;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JTextArea;

import modeles.Modele;

public class VueReponse extends JPanel implements Observer {

	private Modele mod;
	private JTextArea area;

	public VueReponse(Modele m) {
		this.mod = m;
		this.setPreferredSize(new Dimension(100, 340));

		this.setLayout(new BorderLayout());
		area = new JTextArea();
		area.setPreferredSize(new Dimension(100, 340));
		area.setBackground(Color.white);
		this.add(area, BorderLayout.CENTER);
		this.mod.addObserver(this);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// this.area.setText(this.mod.getReponse());
		// String s= mod.getReponse();
		System.out.println("update");
		String s = mod.getReponse();
		System.out.println(s);
		// String ancienTexte=this.area.getText();
		// this.area.setText(ancienTexte+"\n"+s);
		this.area.setText(s);
	}
}
