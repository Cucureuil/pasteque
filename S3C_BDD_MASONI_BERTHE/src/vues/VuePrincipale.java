package vues;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modeles.Modele;

public class VuePrincipale extends JPanel {

	public VuePrincipale() {
		JFrame f = new JFrame("Programme JDBC");
		f.setSize(600, 600);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);

		// MODELE

		Modele m = new Modele();

		try {
			// A VIRER
			String serverName = "localhost";
			// Attention, sous MAMP, le port est 8889
			String portNumber = "3306";

			// iL faut une base nommee testPersonne !
			String dbName = "projet_bdd_s3";

			// creation de la connection
			Properties prop = new Properties();
			prop.put("user", "root");
			prop.put("password", "");
			String urlDB = "jdbc:mysql://" + serverName + ":";
			urlDB += portNumber + "/" + dbName;
			Connection conn = DriverManager.getConnection(urlDB, prop);
			// JUSQUE LA
			/*
			 * String urlDB="jdbc:oracle:thin:@charlemagne:1521:infodb"; String
			 * util="masoni4u"; String mdp="MotDePassePro";
			 * Class.forName("oracle.jdbc.driver.OracleDriver"); Connection
			 * conn; conn = DriverManager.getConnection(urlDB,util,mdp);
			 */
			m.setConnection(conn);
		} catch (Exception e) {
			System.out.println("erreur de connexion");
		}

		// JPanel couvrant toute la fen�tre
		JPanel panneauPrincipal = new JPanel();
		panneauPrincipal.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		panneauPrincipal.setLayout(new BorderLayout());
		panneauPrincipal.setPreferredSize(new Dimension(600, 600));

		// Pannel Top
		JPanel panneau_haut = new JPanel();
		panneau_haut.setPreferredSize(new Dimension(150, 150));
		panneau_haut.setBackground(Color.LIGHT_GRAY);

		// Pannel du gestionnaire
		JPanel vue = new VueGestionAction(m);

		// Panel affichage des r�ponses en bas
		JPanel panneau_bas = new VueReponse(m);

		panneauPrincipal.add(panneau_haut, BorderLayout.NORTH);
		panneauPrincipal.add(vue, BorderLayout.CENTER);
		panneauPrincipal.add(panneau_bas, BorderLayout.SOUTH);

		f.setLocationRelativeTo(null);
		f.add(panneauPrincipal);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
