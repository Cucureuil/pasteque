package controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import modeles.Modele;

public class ControleBouton implements ActionListener {

	private Modele mod;

	public ControleBouton(Modele m) {
		this.mod = m;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try {

			this.mod.methodeAexecuter();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
