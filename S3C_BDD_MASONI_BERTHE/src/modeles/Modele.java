package modeles;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Observable;

import programme.*;

public class Modele extends Observable {

	/**
	 * ATTRIBUTS
	 */

	private int programme_courant;
	private String parametre;
	private appli_JDBC classe;
	private Connection co;
	private String reponse;

	public Modele() {
		this.programme_courant = 1;
		this.classe = new appli_JDBC();
		this.reponse = "";
	}

	public void methodeAexecuter() throws IOException {
		try {
			switch (this.programme_courant) {
			case 1:
				this.reponse = this.classe.listeArticles(this.co, this.parametre);
				break;
			case 2:
				this.reponse = this.classe.listeCoAuteurs(this.co, this.parametre);
				break;
			case 3:
				this.reponse = this.classe.listeLabo(this.co, this.parametre);
				break;
			case 4:
				this.reponse = this.classe.nombreAnnotations(this.co, Integer.parseInt(this.parametre));
				break;
			case 5:
				this.reponse = this.classe.moyenneNotes(this.co, this.parametre);
				break;
			case 6:
				this.reponse = this.classe.infosLabo(this.co, this.parametre);
				break;
			case 7:
				this.reponse = this.classe.noteMax(this.co, this.parametre);
				break;
			default:
				System.out.println("Aucune m�thode � executer");
			}
		} catch (Exception e) {
			System.out.println("Une erreur s'est produite lors du chargement de la m�thode");
		}

		setChanged();
		notifyObservers();

	}

	public void setParametre(String p) {
		this.parametre = p;
	}

	public String getParametre() {
		return this.parametre;
	}

	public void setProgrammeCourant(int i) {
		this.programme_courant = i;
	}

	public int getProgrammeCourant() {
		return this.programme_courant;
	}

	public String getReponse() {
		return this.reponse;
	}

	public void setConnection(Connection c) throws SQLException {
		this.co = c;
		System.out.println(co.isClosed());
	}
}
